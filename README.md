# Create for read authorized_keys data

**Functions:**

- `get_authorized_keys_from_file()`

**Types:**

- `AuthorizedKey`
