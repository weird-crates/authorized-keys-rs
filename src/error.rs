use std::io;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AuthorizedKeysParserError {
    #[error("io error")]
    IOError(#[from] io::Error),

    #[error("unsupported format")]
    UnsupportedFormat
}
